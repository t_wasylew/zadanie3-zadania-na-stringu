import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        String s1 = "Regular expressions are extremely useful as as as as as  in extracting information from text such as code";

        System.out.println(s1.length());
        System.out.println(s1.charAt(0));
        System.out.println(s1.indexOf("W"));
        System.out.println(s1.indexOf("o", 5));
        System.out.println(s1.substring(0, 5));
        System.out.println(s1.toLowerCase());
        System.out.println(s1.toUpperCase());
        System.out.println(s1.equalsIgnoreCase("Hello World!"));
        System.out.println(s1.startsWith("Hello"));
        System.out.println(s1.endsWith("!"));
        System.out.println(s1.contains("World"));
        System.out.println(s1.replace("Regular", "Java"));
        System.out.println(s1.trim());

        String s3 = " Tomasz";
        String s4 = " Wasylew";
        System.out.println(stringConcatWithDelete(s3, s4));
        stringContainsAndHowMany(s1);
        String s = new String("Kajak");
        System.out.println(checkIfPalindrome(s));

        Scanner scanner = new Scanner(System.in);
        System.out.println("Write text:");
        String s2 = scanner.nextLine();
        Pattern pattern = Pattern.compile("([0-9]+)");
        Matcher matcher = pattern.matcher(s2);
        int sum = 0;
        while (matcher.find()) {
            sum += Integer.parseInt(matcher.group());
        }
        System.out.println("Your sum is: " + sum);
    }

    private static boolean checkIfPalindrome(String s) {
        StringBuilder builder = new StringBuilder(s.toLowerCase());
        return builder.equals(builder.reverse());

    }

    private static void stringContainsAndHowMany(String s1) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write word");
        String s = scanner.nextLine();
        System.out.println(s1.contains(s));

        int counter = 0;
        int start = 0;
        for (int i = 0; i < s1.length(); i++) {
            if (s1.substring(start, i).contains(s)) {
                counter++;
                start = i;
            }
        }
        System.out.println("Ilosc wystapien" + counter);
    }


    private static String stringConcatWithDelete(String s3, String s4) {
        return s3.substring(1) + s4.substring(1);
    }
}
